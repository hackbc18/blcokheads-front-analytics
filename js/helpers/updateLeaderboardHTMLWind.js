function updateLeaderboardHTMLWind(rankedList, size) {

    var html = '';

    for (var i = 0; i < rankedList.length; i++) {

        var curSpeed = rankedList[i].speed;
        var maxSpeed = rankedList[i].maxSpeed;
        var dirText = rankedList[i].directionText;
        var rank = size - (9 - i);

        var listElement = '<tr href="#" ><td class="iota__rank">' + rank + '</td><td class="iota__current_Speed">' + curSpeed + '</td><td class="max_Speed">' + maxSpeed + '</td><td class="direction_Text">' + dirText + '</td></tr>'
        html += listElement;
    }

    $("#leaderboard").html(html);

    $("#leaderBoardHead").html('<tr><th class="rank__title">#</th><th class="current_Speed">Speed</th><th class="max_Speed">max. Speed</th><th class="direction_Text">Direction</th></tr>');
}

function buildTransferListWind(list, iota) {
    var newList = [];

    list.forEach(function (transfer) {
        try {

            var message = iota.utils.extractJson([transfer]);
            //console.log(message);

            message = JSON.parse(message).data.split(";");
            if (message) {
                //console.log("JSON: ", message);
            }

            var newTx = {};
            newTx.name = message[0];
            newTx.speed = message[1];
            newTx.maxSpeed = message[2];
            newTx.directionDegree = message[3];
            newTx.directionText = message[4];


            newList.push(newTx);

        } catch (e) {
            console.log(e);
        }
    });

    return newList;
}

function buildChartWind(transferData, transferList) {

    $("#chart").html('<div class="ct-chart ct-perfect-fourth"></div><span style="background-color:lightgreen; margin: 1rem">current wind speed</span><span style="background-color:lightblue; margin: 1rem">max wind speed</span>');

    var chartLable = [];
    var chartwindSpeed = [];
    var chartwindMax = [];
    var chartDeg = [];

    var max = 0;

    for (var i = 0; i < transferList.length; i++) {
        chartLable.push(transferData[i].timestamp);
        chartwindSpeed.push(transferList[i].speed);
        chartwindMax.push(transferList[i].maxSpeed);
        chartDeg.push(transferList[i].directionDegree);

        if (transferList[i].speed > max) {
            max = transferList[i].speed;
        }
        if (transferList[i].maxSpeed > max) {
            max = transferList[i].maxSpeed;
        }
    }

    var chart = new Chartist.Line('.ct-chart', {
        series: [
            chartwindSpeed,
            chartwindMax
        ]
    }, {
        fullWidth: true,
        showPoint: false,
        chartPadding: {
            right: 40
        },
        axisX: {
            showLabel: false,
            showGrid: false
        }
    });

    chart.on('draw', function (context) {
        if (context.type === 'point' && Chartist.getMultiValue(context.value) < (max * 0.4)) {
            context.element.attr({
                style: 'stroke: hsl(' + Math.floor(Chartist.getMultiValue(context.value) / max * 100) + ', 50%, 50%);'
            });
        }
    });


}
