function updateLeaderboardHTMLStuff(rankedList, size) {

    var html = '';

    for (var i = 0; i < rankedList.length; i++) {

        var weather = rankedList[i].weather;
        var roadTemp = rankedList[i].roadTemp;
        var protTemp = rankedList[i].protTemp;
        var saltConc = rankedList[i].saltConc;
        var water = rankedList[i].water;
        var rank = size - (10 - i);

        var listElement = '<tr href="#" ><td class="iota__rank">' + rank + '</td><td class="iota__weather">' + weather + '</td><td class="roadTemp">' + roadTemp + '</td><td class="protTemp">' + protTemp + '</td><td class="saltConc">' + saltConc + '</td><td class="iota__value">' + water + '</td></tr>'
        html += listElement;
    }

    $("#leaderboard").html(html);

    $("#leaderBoardHead").html('<tr><th class="rank__title">#</th><th class="iota__weather">Status</th><th class="roadTemp">Road Temp.</th><th class="protTemp">Protection Temp.</th><th class="saltConc">Salt Concentration</th><th class="full__title">Water in mm</th></tr>');
}

//"data":"RoadSurfaceConditionInformation;;;;;2018-02-03T21:26:47.364+01:00;wet;0.5;-1.0;28.48;0.0;;;;;;\r"
// measurementOrCalculationTime, weatherRelatedRoadConditionType, roadSurfaceTemperature/temperature,
// protectionTemperature/temperature, deIcingConcentration/kilogramsConcentration, waterFilmThickness/floatingPointMetreDistance

function buildTransferLisStuff(list, iota) {
    var newList = [];

    list.forEach(function (transfer) {
        try {

            var message = iota.utils.extractJson([transfer]);
            //console.log(message);
            message = JSON.parse(message).data.split(";");
            if (message) {
                //console.log("JSON: ", message);
            }

            var newTx = {};
            newTx.name = message[0];
            newTx.time = message[5];
            newTx.weather = message[6];
            newTx.roadTemp = message[7];
            newTx.protTemp = message[8];
            newTx.saltConc = message[9];
            newTx.water = message[10];


            newList.push(newTx);

        } catch (e) {
            console.log(e);
        }
    });

    return newList;
}

function buildChartStuff(transferData, transferList) {

    $("#chart").html('<div class="ct-chart ct-perfect-fourth"></div><span style="background-color:lightgreen; margin: 1rem">Protection Temperature</span><span style="background-color:lightblue; margin: 1rem">Road Temperature</span><span style="background-color:#ff9999; margin: 1rem">Water in mm</span>');

    var chartRoadTemp = [];
    var chartProtTemp = [];
    var chartWater = [];

    var max = 0;

    for (var i = 0; i < transferList.length; i++) {
        chartRoadTemp.push(transferList[i].roadTemp);
        chartProtTemp.push(transferList[i].protTemp);
        chartWater.push(transferList[i].water);

        if (transferList[i].roadTemp > max) {
            max = transferList[i].roadTemp;
        }
        if (transferList[i].protTemp > max) {
            max = transferList[i].protTemp;
        }
    }

    var chart = new Chartist.Line('.ct-chart', {
        series: [
            chartRoadTemp,
            chartProtTemp,
            chartWater
        ]
    }, {
        //showLine: false,
        fullWidth: true,
        showPoint: false,
        chartPadding: {
            right: 40

        },
        axisX: {
            showLabel: false,
            showGrid: false
        }
    });

    /*chart.on('draw', function (context) {
        if (context.type === 'point' && Chartist.getMultiValue(context.value) < (max*0.4)) {
            context.element.attr({
                style: 'stroke: hsl(' + Math.floor(Chartist.getMultiValue(context.value) / max * 100) + ', 50%, 50%);'
            });
        }
    });*/
}
