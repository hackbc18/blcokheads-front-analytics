function updateLeaderboardHTMLTemp(rankedList, size) {

    var html = '';

    for (var i = 0; i < rankedList.length; i++) {

        var valueType = rankedList[i].value;
        var rank = size - (10 - i);

        var listElement = '<tr href="#" ><td class="iota__rank">' + rank + '</td><td class="roadTemp">' + valueType + '</td>';
        html += listElement;
    }

    $("#leaderboard").html(html);

    $("#leaderBoardHead").html('<tr><th class="rank__title">#</th><th class="iota__weather">Temperature in °C</th>');
}

function buildTransferListTemp(list, iota) {
    var newList = [];

    list.forEach(function (transfer) {
        try {

            var message = iota.utils.extractJson([transfer]);
            //console.log(message);

            message = JSON.parse(message).data;
            if (message) {
                //console.log("JSON: ", message);
            }

            var newTx = {};
            newTx.value = parseFloat(message);

            newList.push(newTx);

        } catch (e) {
            console.log(e);
        }
    });

    return newList;
}

function buildChartTemp(transferData, transferList) {

    var html = '<a href="#" id="tempInTwoDisplay" style="color: black" class="mainbar-anchor">Based on Mean Algorithmic Input Data</a>';

    $("#chart").html('<div class="ct-chart ct-perfect-fourth"></div>' + html);

    var chartTemp = [];

    var max = -90;

    for (var i = 0; i < transferList.length; i++) {
        if ( typeof transferList[i].value !== 'object') {
            chartTemp.push(transferList[i].value);
        }

        if(max < transferList[i].value){
            max = transferList[i].value;
        }
    }

    var chart = new Chartist.Line('.ct-chart', {
        series: [
            chartTemp
        ]
    }, {
        high: max + 1,
        fullWidth: true,
        showPoint: false,
        chartPadding: {
            right: 40

        },
        axisX: {
            showLabel: false,
            showGrid: false
        }
    });

    return html;
}
