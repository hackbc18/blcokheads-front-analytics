$(document).ready(function () {

    const WIND_DATA_ADDRESS = 'KIDOKSZRCYCDCUHHHAXFGPZMGZYUQSFAVE9NFJRZSPF9QYNOATCTKXCJSLGYSV9UETBVSHGMCKGCFYCUW';
    const WIND_DATA_INDICATOR = 'WIND_BIATCH';

    const STUFF_DATA_ADDRESS = 'XORRPM9IAYEHNFJKSPXQUDFIJQ9MENZUPLGATXJNUGKFCBERO9OPNWREELDKTOFIGUAR9WWUUJLYVKNJW';
    const STUFF_DATA_INDICATOR = 'STUFF_BIATCH';

    const SMART_DATA_ADDRESS = 'DSQPWWTJ9ILTCWLLUFUBDSGSSRORMNBCUMOUKCMJKRXIMTMYOC9SSOISDAGVTHWUK9XKTZVSYRYNJRADC';
    const SMART_DATA_INDICATOR = 'SMART_BIATCH';

    const TEMP_DATA_ADDRESS = 'UAC9DWTADHBGYEWPFBVIEGSK9NBBYWXYDCKAEQRBAYQGGBHMCUZUXX9PPSLCJXYVFYFHLOPOBFQDFVBQX';
    const TEMP_DATA_INDICATOR = 'TEMP_BIATCH';

    const TEMP_IN_DATA_ADDRESS = 'YMII9GTOCWOHWG9XSUWGWZJRNVAETPBZYUARNMNQHFSRKAYDSVUVUGNCMJFFBYQZKNWMGIHNCDVAMF9BY';
    const TEMP_IN_DATA_INDICATOR = 'TEMP_IN_BIATCH';

    var iota = new IOTA({
        'provider': 'https://cloud.hahnpro.com/node'
    });
    var seed;

    var uiDataIndicator = WIND_DATA_INDICATOR;

    var transferDataWind;
    var transferDataStuff;
    var transferDataSmart;
    var transferDataTemp;
    var transferDataTempIn;
    var transferList;
    var checkedTxs = 0;

    function toggleSidebar() {
        $(".button").toggleClass("active");
        $("main").toggleClass("move-to-left");
        $(".sidebar-item").toggleClass("active");
        $(".sidebar").toggleClass("donotdisplay");
    }

    function setSeed(value) {

        seed = "";
        value = value.toUpperCase();

        for (var i = 0; i < value.length; i++) {
            if (("9ABCDEFGHIJKLMNOPQRSTUVWXYZ").indexOf(value.charAt(i)) < 0) {
                seed += "9";
            } else {
                seed += value.charAt(i);
            }
        }
    }

    function getAccountData() {
        console.clear();
        console.log("Getting account info");

        iota.api.findTransactionObjects({
                'addresses': [WIND_DATA_ADDRESS]
            },
            function (e, accountData) {
                accountData.sort(timesort);
                transferDataWind = accountData;
                if (uiDataIndicator === WIND_DATA_INDICATOR) {
                    processList(transferDataWind);
                }
            });
        iota.api.findTransactionObjects({
                'addresses': [STUFF_DATA_ADDRESS]
            },
            function (e, accountData) {
                accountData.sort(timesort);
                transferDataStuff = accountData;
                if (uiDataIndicator === STUFF_DATA_INDICATOR) {
                    processList(transferDataStuff);
                }
            });
        iota.api.findTransactionObjects({
                'addresses': [SMART_DATA_ADDRESS]
            },
            function (e, accountData) {
                accountData.sort(timesort);
                transferDataSmart = accountData;
                if (uiDataIndicator === SMART_DATA_INDICATOR) {
                    processList(transferDataSmart);
                }
            });
        iota.api.findTransactionObjects({
                'addresses': [TEMP_DATA_ADDRESS]
            },
            function (e, accountData) {
                accountData.sort(timesort);
                transferDataTemp = accountData;
                if (uiDataIndicator === TEMP_DATA_INDICATOR) {
                    processList(transferDataTemp);
                }
            });
        iota.api.findTransactionObjects({
                'addresses': [TEMP_IN_DATA_ADDRESS]
            },
            function (e, accountData) {
                accountData.sort(timesort);
                transferDataTempIn = accountData;
                if (uiDataIndicator === TEMP_IN_DATA_INDICATOR) {
                    processList(transferDataTempIn);
                }
            });
        //TODO else if
    }

    function processList(list) {
        transferList = [];

        if (uiDataIndicator === WIND_DATA_INDICATOR) {
            transferList = buildTransferListWind(list, iota);
        } else if (uiDataIndicator === STUFF_DATA_INDICATOR) {
            transferList = buildTransferLisStuff(list, iota);
        } else if (uiDataIndicator === SMART_DATA_INDICATOR) {
            transferList = buildTransferLisSmart(list, iota);
        } else if (uiDataIndicator === TEMP_DATA_INDICATOR) {
            transferList = buildTransferListTemp(list, iota);
        }else if (uiDataIndicator === TEMP_IN_DATA_INDICATOR) {
            transferList = buildTransferListTempIn(list, iota);
        }
        //TODO else if


        checkedTxs = list.length;
        //console.log(transferList);

        var minus = transferList.length;
        if (transferList.length >= 10) minus = 10;


        if (uiDataIndicator === WIND_DATA_INDICATOR) {
            updateLeaderboardHTMLWind(transferList.slice(transferList.length - minus, transferList.length), transferList.length);
            buildChartWind(list, transferList);
        }
        else if (uiDataIndicator === STUFF_DATA_INDICATOR) {
            updateLeaderboardHTMLStuff(transferList.slice(transferList.length - minus, transferList.length), transferList.length);
            buildChartStuff(list, transferList);
        }
        else if (uiDataIndicator === SMART_DATA_INDICATOR) {
            updateLeaderboardHTMLSmart(transferList.slice(transferList.length - minus, transferList.length), transferList.length);
            buildChartSmart(list, transferList);
        }
        else if (uiDataIndicator === TEMP_DATA_INDICATOR) {
            updateLeaderboardHTMLTemp(transferList.slice(transferList.length - minus, transferList.length), transferList.length);
            buildChartTemp(list, transferList);
            $("#tempInTwoDisplay").on("click", function () {
                uiDataIndicator = TEMP_IN_DATA_INDICATOR;
                console.log("Now displaying:", uiDataIndicator);
                processList(transferDataTempIn);
            });
        }
        else if (uiDataIndicator === TEMP_IN_DATA_INDICATOR) {
            updateLeaderboardHTMLTempIn(transferList.slice(transferList.length - minus, transferList.length), transferList.length);
            buildChartTempIn(list, transferList);

        }
        //TODO else if

    }

    $("#seedSubmit").on("click", function () {

        setSeed("ACAADSGSAATA9BGETDSDDSFJAAADSDHBSHBFDHJSBHJDBDHJFJHDGHJFG9999DGHJFGDJHHJGFHDBHJDSBHBDHSHDBHSHDBHJSB");

        var address = $("#userSeed").val();
        console.dir(address);

        if (address === WIND_DATA_ADDRESS) {
            uiDataIndicator = WIND_DATA_INDICATOR
        } else if (address === STUFF_DATA_ADDRESS) {
            uiDataIndicator = STUFF_DATA_INDICATOR
        } else if (address === SMART_DATA_ADDRESS) {
            uiDataIndicator = SMART_DATA_INDICATOR
        } else if (address === TEMP_DATA_ADDRESS) {
            uiDataIndicator = TEMP_DATA_INDICATOR
        }else if (address === TEMP_IN_DATA_ADDRESS) {
            uiDataIndicator = TEMP_IN_DATA_INDICATOR
        } else {
            return;
        }

        $("#leaderboardSection").html('<table class="table-fill"><thead id="leaderBoardHead"><tr><th class="rank__title">-</th><th class="middle_title">Description</th><th class="full__title">-</th></tr></thead> <tbody id="leaderboard"><tr> <td class="iota__rank"></td> <td class="iota__direction_Text">Getting Data, please wait...</td> <td class="iota__value"></td> </tr> </tbody> </table> <table class="table-fill" style="max-width: 1200px;"> <thead> <tr> <th class="middle_title">Table</th> </tr> </thead> <tbody> <tr> <td class="iota__direction_Text" id="chart">Getting Data, please wait...</td> </tr> </tbody> </table>');

        // We fetch the latest transactions every 5 minutes
        getAccountData();
        setInterval(getAccountData, 60000);
    });

    $("#windSensor").on("click", function () {
        setSeed("ACAADSGSAATA9BGETDSDDSFJAAADSDHBSHBFDHJSBHJDBDHJFJHDGHJFG9999DGHJFGDJHHJGFHDBHJDSBHBDHSHDBHSHDBHJSB");

        uiDataIndicator = WIND_DATA_INDICATOR;

        $("#leaderboardSection").html('<table class="table-fill"><thead id="leaderBoardHead"><tr><th class="rank__title">-</th><th class="middle_title">Description</th><th class="full__title">-</th></tr></thead> <tbody id="leaderboard"><tr> <td class="iota__rank"></td> <td class="iota__direction_Text">Getting Data, please wait...</td> <td class="iota__value"></td> </tr> </tbody> </table> <table class="table-fill" style="max-width: 1200px;"> <thead> <tr> <th class="middle_title">Table</th> </tr> </thead> <tbody> <tr> <td class="iota__direction_Text" id="chart">Getting Data, please wait...</td> </tr> </tbody> </table>');

        // We fetch the latest transactions every 5 minutes
        getAccountData();
        setInterval(getAccountData, 300000);
    });

    $("#roadSensor").on("click", function () {
        setSeed("ACAADSGSAATA9BGETDSDDSFJAAADSDHBSHBFDHJSBHJDBDHJFJHDGHJFG9999DGHJFGDJHHJGFHDBHJDSBHBDHSHDBHSHDBHJSB");

        uiDataIndicator = STUFF_DATA_INDICATOR;

        $("#leaderboardSection").html('<table class="table-fill"><thead id="leaderBoardHead"><tr><th class="rank__title">-</th><th class="middle_title">Description</th><th class="full__title">-</th></tr></thead> <tbody id="leaderboard"><tr> <td class="iota__rank"></td> <td class="iota__direction_Text">Getting Data, please wait...</td> <td class="iota__value"></td> </tr> </tbody> </table> <table class="table-fill" style="max-width: 1200px;"> <thead> <tr> <th class="middle_title">Table</th> </tr> </thead> <tbody> <tr> <td class="iota__direction_Text" id="chart">Getting Data, please wait...</td> </tr> </tbody> </table>');

        // We fetch the latest transactions every 5 minutes
        getAccountData();
        setInterval(getAccountData, 300000);
    });

    $("#smartSensor").on("click", function () {
        setSeed("ACAADSGSAATA9BGETDSDDSFJAAADSDHBSHBFDHJSBHJDBDHJFJHDGHJFG9999DGHJFGDJHHJGFHDBHJDSBHBDHSHDBHSHDBHJSB");

        uiDataIndicator = SMART_DATA_INDICATOR;

        $("#leaderboardSection").html('<table class="table-fill"><thead id="leaderBoardHead"><tr><th class="rank__title">-</th><th class="middle_title">Description</th><th class="full__title">-</th></tr></thead> <tbody id="leaderboard"><tr> <td class="iota__rank"></td> <td class="iota__direction_Text">Getting Data, please wait...</td> <td class="iota__value"></td> </tr> </tbody> </table> <table class="table-fill" style="max-width: 1200px;"> <thead> <tr> <th class="middle_title">Table</th> </tr> </thead> <tbody> <tr> <td class="iota__direction_Text" id="chart">Getting Data, please wait...</td> </tr> </tbody> </table>');

        // We fetch the latest transactions every 5 minutes
        getAccountData();
        setInterval(getAccountData, 300000);
    });

    $("#tempSensor").on("click", function () {
        setSeed("ACAADSGSAATA9BGETDSDDSFJAAADSDHBSHBFDHJSBHJDBDHJFJHDGHJFG9999DGHJFGDJHHJGFHDBHJDSBHBDHSHDBHSHDBHJSB");

        uiDataIndicator = TEMP_DATA_INDICATOR;

        $("#leaderboardSection").html('<table class="table-fill"><thead id="leaderBoardHead"><tr><th class="rank__title">-</th><th class="middle_title">Description</th><th class="full__title">-</th></tr></thead> <tbody id="leaderboard"><tr> <td class="iota__rank"></td> <td class="iota__direction_Text">Getting Data, please wait...</td> <td class="iota__value"></td> </tr> </tbody> </table> <table class="table-fill" style="max-width: 1200px;"> <thead> <tr> <th class="middle_title">Table</th> </tr> </thead> <tbody> <tr> <td class="iota__direction_Text" id="chart">Getting Data, please wait...</td> </tr> </tbody> </table>');

        // We fetch the latest transactions every 5 minutes
        getAccountData();
        setInterval(getAccountData, 300000);
    });

    $("#tempInSensor").on("click", function () {
        setSeed("ACAADSGSAATA9BGETDSDDSFJAAADSDHBSHBFDHJSBHJDBDHJFJHDGHJFG9999DGHJFGDJHHJGFHDBHJDSBHBDHSHDBHSHDBHJSB");

        uiDataIndicator = TEMP_IN_DATA_INDICATOR;

        $("#leaderboardSection").html('<table class="table-fill"><thead id="leaderBoardHead"><tr><th class="rank__title">-</th><th class="middle_title">Description</th><th class="full__title">-</th></tr></thead> <tbody id="leaderboard"><tr> <td class="iota__rank"></td> <td class="iota__direction_Text">Getting Data, please wait...</td> <td class="iota__value"></td> </tr> </tbody> </table> <table class="table-fill" style="max-width: 1200px;"> <thead> <tr> <th class="middle_title">Table</th> </tr> </thead> <tbody> <tr> <td class="iota__direction_Text" id="chart">Getting Data, please wait...</td> </tr> </tbody> </table>');

        // We fetch the latest transactions every 5 minutes
        getAccountData();
        setInterval(getAccountData, 300000);
    });

    $("#windDisplay").on("click", function () {

        uiDataIndicator = WIND_DATA_INDICATOR;
        console.log("Now displaying:", uiDataIndicator);
        processList(transferDataWind);
    });

    $("#roadDisplay").on("click", function () {

        uiDataIndicator = STUFF_DATA_INDICATOR;
        console.log("Now displaying:", uiDataIndicator);
        processList(transferDataStuff);
    });

    $("#smartDisplay").on("click", function () {

        uiDataIndicator = SMART_DATA_INDICATOR;
        console.log("Now displaying:", uiDataIndicator);
        processList(transferDataSmart);
    });

    $("#tempDisplay").on("click", function () {

        uiDataIndicator = TEMP_DATA_INDICATOR;
        console.log("Now displaying:", uiDataIndicator);
        processList(transferDataTemp);
    });

    $("#tempInDisplay").on("click", function () {

        uiDataIndicator = TEMP_IN_DATA_INDICATOR;
        console.log("Now displaying:", uiDataIndicator);
        processList(transferDataTempIn);
    });

    $("#btn").on("click tap", function () {
        toggleSidebar();
    });

    function timesort(o1, o2) {
        if (o1.timestamp < o2.timestamp)    return -1;
        else if (o1.timestamp > o2.timestamp) return 1;
        else             return 0;
    }
});
